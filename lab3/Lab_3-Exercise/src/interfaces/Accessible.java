package interfaces;

public interface Accessible {
	public String getName();
	public void setName(String name);
	public String getId();
}
