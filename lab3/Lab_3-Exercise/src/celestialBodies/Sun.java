package celestialBodies;

import Exceptions.MoreThanLimitException;
import Exceptions.RadiusException;
import shapes.*;

public class Sun extends CelestialBody {
	private String sunId;
	private static int counter = 0;
	private double min_distance;
	private int num_planets;
	public Sun(Point center,double radius, String sunName,double min_distance, Planet[]p) throws MoreThanLimitException, RadiusException{
		super(center, radius, sunName);
		sunId = "S"+ (++counter);
		this.min_distance=min_distance;
		if(p.length>5)
			throw new MoreThanLimitException("Sun has too many planets associated with object: "+sunId);
		else
			num_planets=p.length;
	}
	
	//Calculables
	public double area() {
		return (4 * Math.PI * Math.pow(radius, 2));
	}
	public double volume() {
		return ((4.0/3.0) * Math.PI * Math.pow(radius, 3));
	}
	public double perimeter() {
		return (2* Math.PI * radius);
	}
	
	//Getters
	public  Point getCenter(){
		return center;
	}
	public int getCounter(){
		return counter;
	}
	public String getId(){
		return sunId;
	}
	public double getMinDistance(){
		return min_distance;
	}
	public int getNumOfPlanets(){
		return num_planets;
	}
	public  double getRadius(){
		return radius;
	}
	public  String getName(){
		return this.celestialName;
	}

	//Setters
	public void setCenter(Point p){
		center=p;
	}
	public void setCounter(int i){
		counter = i;
	}
	public void setIdNum(String id){
		sunId = id;
	}
	public void setMinDistance(double dist){
		min_distance=dist;
	}
	public void setNumOfPlanets(int i){
		num_planets = i;
	}
	public void setName(String name){
		celestialName=name;
	}
	public void setRadius(double radius){
		this.radius=radius;
	}
	
	
	public String toString(){
		return ("Name: \"Sun: "+ getName()+"\" Object Id: "+getId()
				+"\n---------------------\nRadius: "
				+getRadius()+"\nArea: "+area()+"\nCenter: "+ getCenter()+"\nMinimum approach distance: "
				+ getMinDistance()+"\n");
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Point p = new Point (1,2);
	}

}
