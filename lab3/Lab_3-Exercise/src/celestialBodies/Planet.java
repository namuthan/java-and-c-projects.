package celestialBodies;

import Exceptions.*;
import shapes.Point;

public class Planet extends CelestialBody {
	private int num_moons;
	private double orbital_distance;
	private String planetId;
	private static int counter = 0;
	
	public Planet(Point center,double radius, String name,double orbital_distance, Moon[] m) throws RadiusException, MoreThanLimitException{
		super(center,radius,name);
		planetId = "PL"+ (++counter);
		this.orbital_distance=orbital_distance;
		if(radius<0)
			throw new RadiusException("Planet has incorrect radius in object: ",this);
		if(m.length>5)
			throw new MoreThanLimitException("Planet has too many moons. Object: "+planetId);
		else
			num_moons = m.length;
	}
	
	
	public Planet(Point center,double radius, String name,double orbital_distance, Moon m) throws RadiusException{
		super(center,radius,name);
		planetId = "PL"+ (++counter);
		if(radius<0)
			throw new RadiusException("Planet has incorrect radius in object: ",this);
		this.orbital_distance=orbital_distance;
		num_moons=1;
	}
	
	//Calculables
	public double area() {
		return (4 * Math.PI * Math.pow(radius, 2));
	}
	public double volume() {
		return ((4.0/3.0) * Math.PI * Math.pow(radius, 3));
	}
	public double perimeter() {
		return (2* Math.PI * radius);
	}

	//Getters
	public  Point getCenter(){
		return center;
	}
	public int getCounter(){
		return counter;
	}	
	public String getId(){
		return planetId;
	}
	public double getOrbitalDistance(){
		return orbital_distance;
	}
	public  String getName(){
		return this.celestialName;
	}
	public int getNumOfMoons(){
		return num_moons;
	}
	public  double getRadius(){
		return radius;
	}

	//Setters
	public void setCounter(int count) {
		counter=count;
	}	
	public  void setName(String name){
		celestialName=name;
	}
	public void setNumOfMoons(int num){
		num_moons=num;
	}
	public void getOrbitalDistance(double num){
		orbital_distance=num;
	}
	public  void setRadius(double radius){
		this.radius=radius;
	}
	
	
	public String toString(){
		return ("Name: \"Planet: "+ getName()+"\" Object Id: "+getId()
				+"\n---------------------\nRadius: "
				+getRadius()+"\nArea: "+area()+"\nCenter: "+ getCenter()+"\nOrbital Distance: "
				+ getOrbitalDistance())+"\nNumber of moons: "+getNumOfMoons()+"\n";
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
