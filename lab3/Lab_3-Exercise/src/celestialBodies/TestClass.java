package celestialBodies;

import interfaces.Accessible;
import interfaces.Calculable;
import Exceptions.MoonRadiusException;
import Exceptions.MoreThanLimitException;
import Exceptions.RadiusException;
import shapes.Circle;
import shapes.Point;

public class TestClass {

	/**
	 * @param args
	 */
	static public void access ( Accessible ac, Calculable ca) {
		//- Call all the functions get, set, and area etc, which is declared in interface Accessible and Calculable.
		System.out.println("--------------------------------\nTesting Accessible with object: "+ac.getId());
		System.out.println("Old name is: "+ac.getName());
		ac.setName("test");
		System.out.println("New name is: " + ac.getName());
		
		System.out.println("area: "+ca.area());
		System.out.println("perimeter: "+ca.perimeter());
		System.out.println("volume: "+ca.volume());
	}
	public static void main(String[] args) {
		// create point x(0,0)
		Point x = new Point(0,0);
		//create point y(12, 21)
		Point y = new Point(12,21);
		//point z copy of x
		Point z;
		try {
			z = (Point)x.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			System.out.flush();
		}
		
		//create an array of 11 points
		Point[] point_array = new Point[11];
		 for(int i=0;i<point_array.length;i++)
			 point_array[i]=new Point(100.00 + i, 200.00 + i);
		
		 //create an array of moons
		Moon[] M = new Moon[10];
		try {
			M[0]=new Moon(point_array[0],1,"HardRockMoon", 443.211);
		} catch (MoonRadiusException e) {
			e.printStackTrace();
			System.out.flush();
		}
		
		try {
			M[1]=new Moon(point_array[1],5,"SoftRockMoon", 434312.3);
		} catch (MoonRadiusException e) {
			e.printStackTrace();
			System.out.flush();
		}
		
		try {
			M[3]=new Moon(point_array[2],1,"MonkeyLand", 9998);
		} catch (MoonRadiusException e) {
			e.printStackTrace();
			System.out.flush();
		}
		try {
			M[4] = new Moon(point_array[7], 11, "BlackRocks", 12);
		} catch (MoonRadiusException e) {
			e.printStackTrace();
			System.out.flush();
			System.out.println("\nMoon BlackRocks was rebuilt. With the radius of 10.");
			try {
				M[4] = new Moon(point_array[7],10,"BlackRocks",12);
			} catch (MoonRadiusException e1) {
				e1.printStackTrace();
				System.out.flush();
			}
			
		} 
		
		//Creates an array of planets
		Planet[] P = new Planet[2];
		try {
			P[0] = new Planet(point_array[7],10001311.0,"H4X0rZ",600.00, M[0]);
			P[1] = new Planet(point_array[8], 10021, "Tatooine", 10.00, M[1]);
			
		} catch (RadiusException e) {
			e.printStackTrace();
			System.out.flush();
		}
		
		//System.out.println(P[1]);
		System.out.println(P[0]);
		System.out.println(P[1]);
		
		//Testing Circles and Cloneable
		Circle big=null;
		try {
			big = new Circle("big", 23, 25, 1000);
		} catch (RadiusException e1) {
			e1.printStackTrace();
			System.out.flush();
		}
		
		Circle small=null;
		try {
			small = new Circle("small", 23, 25, 10);
		} catch (RadiusException e1) {
			e1.printStackTrace();
			System.out.flush();
		}
		
		Circle copy = null;
		try {
			copy = (Circle)small.clone();
			System.out.println (copy);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			System.out.flush();
		}
			
		//Test Create a Sun object, its planets, and their moons
		Sun S = null;
		try {
			S = new Sun(point_array[9],1123, "the SUN",10009999,P);
			//Test toString() method on S
			System.out.println(S);
			Sun BlackHole = (Sun) S.clone();
			S.setRadius(144.00);
			System.out.println(S);
			System.out.println(BlackHole);
		} catch (MoreThanLimitException e) {
			e.printStackTrace();
			System.out.flush();
		} catch (RadiusException e) {
			e.printStackTrace();
			System.out.flush();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			System.out.flush();
		}
		
		access(S, S);
		access(M[0], M[0]);
		access(P[0], P[0]);
		access(small, small);

		
		
	}

}
