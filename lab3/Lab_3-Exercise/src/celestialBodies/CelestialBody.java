package celestialBodies;

import interfaces.*;
import Exceptions.RadiusException;
import shapes.Point;

public abstract class CelestialBody implements Accessible,Calculable,Cloneable {
	protected Point center; 
	protected double radius;
	private String celestialId;
	protected String celestialName;
	private static int counter=0;
	public CelestialBody(Point center, double radius,String name) {
		this.center=center;
		this.celestialName=name;
		counter++;
		this.celestialId= "CB" + counter;
		this.radius=radius;
	}
	
	//Calculables
	public abstract double area();
	public abstract double volume();
	public abstract double perimeter();
	
	//Getters
	public abstract int getCounter();
	public abstract Point getCenter();
	public  String getCelestialId(){
		return celestialId;
	}
	public  int getCelestialCount(){
		return counter;
	}
	public abstract String getId();
	public abstract String getName();
	public abstract double getRadius();
	
	//Setters
	public abstract void setCounter(int count);
	public abstract void setName(String name);
	public abstract void setRadius(double radius);
	
	
	public Object clone() throws CloneNotSupportedException{
		CelestialBody obj = (CelestialBody)super.clone();
		obj.center = (Point)center.clone();
		return obj;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
