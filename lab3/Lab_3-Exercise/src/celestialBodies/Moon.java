
package celestialBodies;
import Exceptions.MoonRadiusException;
import Exceptions.RadiusException;
import shapes.*;


public class Moon extends CelestialBody {
	private double distance_from_planet;
	private String moonId;
	private static int counter = 0;
	
	public Moon(Point center, double radius,String moonName, double distance_from_planet) throws MoonRadiusException{
		super(center,radius,moonName);
		moonId = "Mo"+ (++counter);
		if((radius<0)||(radius>10)){
			throw new MoonRadiusException("Moon radius out of range (0 � 10), in object: ",this);
		}
		this.distance_from_planet=distance_from_planet;
	}
	
	//Calculables
	public double area() {
		return (4 * Math.PI * Math.pow(radius, 2));
	}
	public double volume() {
		return ((4.0/3.0) * Math.PI * Math.pow(radius, 3));
	}
	public double perimeter() {
		return (2* Math.PI * radius);
	}

	//Getters
	public  Point getCenter(){
		return center;
	}
	public int getCounter() {
		return counter;
	}
	public double getDistanceFromPlanet(){
		return distance_from_planet;
	}
	public String getId(){
		return moonId;
	}
	public  String getName(){
		return this.celestialName;
	}
	public  double getRadius(){
		return radius;
	}

	//Setters
	public void setCounter(int count) {
		counter=count;
	}
	public  void setName(String name){
		celestialName=name;
	}
	public  void setRadius(double radius){
		this.radius=radius;
	}
	
	public String toString(){
		return ("Name: \"Moon: "+ getName()+"\" Object Id: "+getId()
				+"\n---------------------\nRadius: "
				+getRadius()+"\nArea: "+area()+"\nCenter: "+ getCenter()+"\nDistance from planet: "
				+ getDistanceFromPlanet()+"\n");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Point p = new Point(100.0, 200.0);
		try {
			Moon Mono = new Moon(p,1,"HardRockMoon", 443.211);
			System.out.println(Mono);
		} catch (MoonRadiusException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
	}



}
