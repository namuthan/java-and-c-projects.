package shapes;

import Exceptions.RadiusException;

public class Circle extends Shape implements Cloneable{
	protected double radius;
	private String shapeId;
	private static int counter = 0;
	
	public Circle(String circName, double x, double y, double radius) throws RadiusException {
		
		super(circName, x, y);
		counter++;
		shapeId="C"+ counter;
		if(radius<0)
			throw new RadiusException("Circle has incorrect radius in object: ",this);
		this.radius=radius;
	}
	
	//Calculables
	public double area(){
		/*
		 * REQUIRES: radius is already intialized as double
		 * PROMISES: returns the area as a double
		 */
		return ((Math.PI)*(Math.pow(radius,2)) );
	}
	public double perimeter(){
		/*
		 * REQUIRES: radius is intialized and double
		 * PROMISES: returns the perimeter as a double
		 */
		return ((2)*(Math.PI)*(radius));
	}
	public double volume(){return 0;}
	
	//setters and getters specific to circle
	public int getCounter(){
		return counter;
	}
	public String getId(){
		return shapeId;
	}
	public String getName(){
		return shapeName;
	}
	public double getRadius(){
		return radius;
	}

	public void setCounter(int count){
		counter=count;
	}
	public void setId(String str){
		shapeId=str;
	}
	public void setName(String name) {
		this.shapeName=name;
	}
	public void setRadius(double radius){
		this.radius=radius;
	}
	
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
	public String toString(){
		return ("Shape name: "+shapeName+"\nShape Id: "+ shapeId+"\nOrigin: "+origin+
				"\nradius: "+radius);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Circle r1 = new Circle ("MY Circle", 1.0, 2.0,2.2);
		//System.out.println(r1.perimeter()+" "+r1.area());
		//System.out.println(r1);

	}
	

}
