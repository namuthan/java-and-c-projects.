package shapes;
public class Point implements Cloneable{
	private double x,y;
	private static int counter=0;
	private String pointId;
	public Point(double x, double y){
		counter++;
		this.x = x;
		this.y = y;
		pointId="P"+counter;
		
	}
	public String toString(){
		return ("Point Id: "+pointId+"\nX-coordinate: "+x+"\nY-coordinate: "+y);
	}
	
	public static double distanceBetweenPoints(Point start, Point end){
		/*Class Method
		 * REQUIRED:two point objects are passed 
		 * PROMISES: returns the distance between the two points
		 */
		return Math.sqrt(Math.pow((start.x-end.x), 2)+Math.pow((start.y-end.y), 2));
	}

	public double distanceBetweenPoints(Point end){
		/* REQUIRED: a point object to be passed in
		 * PROMISES: compares point object with the point that called this method.
		 * returns the distance between the two points
		 */
		return Math.sqrt(Math.pow((this.x-end.x), 2)+Math.pow((this.y-end.y), 2));
	}
	
	//Setter and getter functions for the data fields of Point
	public double getX(){return x;}
	public double getY(){return y;}
	public static int count(){return counter;}
	public void setX(double x){this.x=x;}
	public void setY(double y){this.y=y;}
	public void setXY(double x, double y){
		this.y=y;
		this.x=x;
	}
	
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Point a=new Point(6,7);
		Point b=new Point(0.8,1.2);
		Point c=new Point(45,66);
		System.out.println(a + "\n"+b +"\n"+c);
		System.out.println("Distance between P1 and P2: "+ a.distanceBetweenPoints(b));
		*/
		Point a, b;
	    a = new Point(3.0, -5.0);
	    try {
	        b = (Point)a.clone();
	        System.out.println("b: x = " + b.getX() +
	                              "y = " + b.getY());
	    }
	    catch(CloneNotSupportedException e) {
	        System.out.println("Can't clone Point a");
	    }
	}
}
