package shapes;
public class Prism extends Rectangle implements Cloneable{
	private double height;
	private String shapeId;
	protected static int counter = 0;
	
	public Prism(String prismName, double x, double y, double width, double length, double height){
		super(prismName, x, y, width, length);
		counter++;
		this.shapeId="PR"+ counter;
		this.height=height;
	}
	
	public double volume(){
		/*
		 * REQUIRES: area() is a method of Rectangle, inherited by Prism. 
		 * PROMISES: returns the volume of the rectangular prism
		 */
		return (area()*height);
	}
	public double area(){
		/*
		 * REQUIRES: that width, length, and height are initialized 
		 * PROMISES: returns surface area
		 */
		return 2*((width*length)+(length*height)+(width*height));
	}
	
	//setters and getters specific to Rectangle
	public static int count(){return counter;}
	public int getCounter(){
		return counter;
	}
	public String getShapeId(){
		return shapeId;
	}
	public void setShapeId(String str){
		shapeId=str;
	}
	public void setCounter(int count){
		counter=count;
	}
	public void setHeight(double height){
		this.height=height;
	}
	
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
	
	public String toString(){
		return ("Shape name: "+shapeName+"\nShape Id: "+ shapeId+"\nOrigin: "+origin+
				"\nWidth: "+width+"\nLength: "+length+"\nHeight: "+height);
	}
	
	public String getId() {
		return shapeId;
	}

	public String getName() {
		return this.getShapeName();
	}

	public void setName(String name) {
		this.setName(name);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shape s1;
		Rectangle r1;
		Prism p1 = new Prism("MY Prism", 1,2,2,2,4);
		//Prism p2 = new Prism("MY Prism", 1,2,2,2,4);
		s1 = p1;
		r1 = p1;
		System.out.print(r1.area());
	}

}
