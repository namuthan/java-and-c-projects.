package shapes;

import interfaces.*;

public abstract class Shape implements Accessible,Calculable {
	protected Point origin;
	protected String shapeName;
	private String shapeId;
	private static int counter = 0;
	public Shape(String shapeName,double x, double y){
		origin = new Point(x,y);
		counter++;
		shapeId="Shape"+counter;
		this.shapeName = shapeName;
	}
	
	
	public abstract double area();


	public abstract double perimeter();


	public abstract double volume();


	public static double distanceBetweenShapes(Shape s1, Shape s2){
		/*Class Method
		 * REQUIRED:two shape objects are passed 
		 * PROMISES: returns the distance between the two shape's origins
		 */
		return Point.distanceBetweenPoints(s1.origin,s2.origin);
	}
	public double distanceBetweenShapes(Shape s1){
		/* REQUIRED: a shape object to be passed in
		 * PROMISES: compares origin of shape passed in with the origin of the shape
		 * that called this method.returns the distance between the two points
		 */
		return this.origin.distanceBetweenPoints(s1.origin);
	}
	public void move(double dx, double dy){
		/*
		 * REQUIRES: dx and dy are valid doubles. dx and dy denote the change for x and y
		 * PROMISES: translates the origin of each shape to x+dx and y+dy
		 */
		origin.setX((origin.getX())+dx);
		origin.setY((origin.getY())+dy);
	}
	
	//Getters
	public int getShapeCounter(){
		return counter;
	}
	public abstract int getCounter();
	public abstract String getId() ;
	public abstract String getName();	
	public Point getOrigin(){
		return origin;
	}
	public String getShapeName(){
		return shapeName;
	}
	public String getShapeId(){
		return shapeId;
	}

	//Setters
	public void setOrigin(int x, int y){
		origin.setXY(x,y);
	}
	public abstract void setName(String name) ;
	public void setShapeName(String str){
		shapeName=str;
	}
	public void setShapeId(String str){
		shapeId=str;
	}
	public void setCounter(int count){
		counter=count;
	}
	

	
	
	public Object clone() throws CloneNotSupportedException{
		Shape obj = (Shape)super.clone();
		obj.origin = (Point)origin.clone();
		return obj;
	}
		
	public String toString(){
		return ("Shape name: "+shapeName+"\nShape Id: "+ shapeId+"\nOrigin: "+origin);
	}
	
	public static void main(String[] args) {
		/*Shape s1 = new Shape("My shape",6,7);
		Shape s2 = new Shape("My shape2",0.8,1.2);
		System.out.println("The distance is:" + Shape.distanceBetweenShapes(s1, s2));
		System.out.println(s2);
		System.out.println("Origin is at:\n"+s1.origin);
		s1.move(2,2);
		System.out.println("Origin is at:\n"+s1.origin);
	*/
	}
}
