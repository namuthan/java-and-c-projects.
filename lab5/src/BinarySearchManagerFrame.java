
import java.awt.Container;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


import javax.swing.*;


public class BinarySearchManagerFrame extends JFrame{
	private static BinSearchTree bst; 
	private JPanel buttonsPanel;
	private JButton insertButton;
	private JButton findButton;
	private JButton browseButton;
	private JButton createButton;
	private JScrollPane screenPanel;
	private JTextArea outputScreen;
	//private Dimension frameDimension;
	private static String titleFrame="Application Main Window: Maintain Student Records";
	private InsertNodeFrame ins;
	private static String inputfile;
	private static Scanner instream;
	private static PrintWriter outstream;
	
	private Container c;
	// ADD MORE COMPONENTS AS NEEDED
	
	public BinarySearchManagerFrame () {
		super(titleFrame);
		this.setLocationByPlatform(true);
		
		// create an empty tree
		bst = new BinSearchTree();
	
		// create and initialize the components
		
		buttonsPanel = new JPanel();
		insertButton = new JButton("Insert");
		findButton = new JButton("Find");
		browseButton = new JButton("Browse");
		createButton = new JButton("Create Tree from File");
		screenPanel = new JScrollPane();
		outputScreen = new JTextArea();
		outputScreen.setEditable(false);
		outputScreen.setTabSize(10);
		
		// get a reference to the frame's container and add
		// a panel to the container
		c = getContentPane();
		
		
		//initializes the elemnets of the frame
		initComponents();
		
		//ButtonHandelers. Has even handlers for the four different buttons
		initButtonHandlers();
		
		//create a window listener to close the program
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				dispose();
			}
			});

	
		screenPanel.setViewportView(outputScreen);
		c.add(screenPanel);
		pack();
		
	
	}

	public void initComponents(){
		//initialize button panel
		buttonsPanel.add(insertButton);
		buttonsPanel.add(findButton);
		buttonsPanel.add(browseButton);
		buttonsPanel.add(createButton);
		c.add(buttonsPanel, "South");
		
		
		outputScreen.setText("This application allows users to:\n" +
				"\t- to maintain list of studetns\n" +
				"\t- to insert a new student to the list\n" +
				"\t- to delete a student from the list\n" +
				"\t- to search for a student using students ID number\n");
		
		
	}
	
	public void initButtonHandlers(){
				// create an anonymous ActionListener object to
				// listen to insert-button event	
				insertButton.addActionListener(new ActionListener()
				{
					public void actionPerformed (ActionEvent evt){
						if(bst.empty()){
							JOptionPane.showMessageDialog(null, "There is no Binary Tree created. Create from file first.", "Warning", JOptionPane.ERROR_MESSAGE);
						}
						else{
						ins = new InsertNodeFrame(bst);
						ins.setVisible(true);
						}
					}
				});
				
				// create an anonymous ActionListener object to
				// listen to find-button event	
				findButton.addActionListener(new ActionListener()
				{
					public void actionPerformed (ActionEvent evt){
						if(bst.empty()){
							JOptionPane.showMessageDialog(null, "There is no Binary Tree created. Create from file first.", "Warning", JOptionPane.ERROR_MESSAGE);
						}
						else{
							String id = JOptionPane.showInputDialog("Please enter the studet's id:");
							Node lookup=bst.find(bst.root, id);
							if(lookup==null)
								JOptionPane.showMessageDialog(null, "There is no user with that id.", "Warning", JOptionPane.ERROR_MESSAGE);
							else
								JOptionPane.showMessageDialog(null, lookup, "Message", JOptionPane.INFORMATION_MESSAGE);
							
						}
					}
				});
				
				// create an anonymous ActionListener object to
				// listen to browse-button event	
				browseButton.addActionListener(new ActionListener()
				{
					public void actionPerformed (ActionEvent evt){
						if(bst.empty()){
							JOptionPane.showMessageDialog(null, "There is no Binary Tree created. Create from file first.", "Warning", JOptionPane.ERROR_MESSAGE);
						}
						else{
						String buffer="";
						String textFill="Name\t\tID\tFaculty\tMajor\tYear\n" +
								"------------------------------------------------------------------------------------\n";
						buffer=bst.print_tree(bst.root,buffer);
						textFill+=buffer;
						System.out.println(textFill);
						outputScreen.setText(textFill);
						outputScreen.setCaretPosition(0);
						}
					}
				});

				// create an anonymous ActionListener object to
				// listen to create-button event	
				createButton.addActionListener(new ActionListener()
				{
					public void actionPerformed (ActionEvent evt){
						String answer=JOptionPane.showInputDialog("The existing data will be deleted.\nDo you wish to continue?\nEnter Yes or No:");
						if(answer.toLowerCase().equals("yes")){
							inputfile = JOptionPane.showInputDialog("Enter the file name:");
							try {
								instream = new Scanner(new File(inputfile));
								read_file();
							} catch (IOException e) {
								JOptionPane.showMessageDialog(null, "File could not be opened. Please try again.", "Warning", JOptionPane.ERROR_MESSAGE);
							}
						}
						
						
					}
					
				});
				
	}
	
	public static void read_file(){
		/*
		 * Reads contents of file
		 */
		String name="";
		String id;
		String faculty;
		String major;
		String year;
		while(instream.hasNext()){
			name="";
			while(instream.hasNextInt()==false){
				name +=instream.next()+" ";
			}
			id = instream.next();
			faculty = instream.next();
			major = instream.next();
			year = instream.next();
			bst.insert(name, id, faculty, major, year);
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BinarySearchManagerFrame demo = new BinarySearchManagerFrame();
		//t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		demo.setVisible(true);
		
	}

}
