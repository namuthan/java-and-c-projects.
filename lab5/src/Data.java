/*
 * Data.java
 *
 * Created on March 16, 2002, 10:00 AM
 */

// package binSearchApp;
import java.util.*;
/**
 *
 * @author  M. Moussavi
 * @version 
 */
public class Data {
	
	String name, id,faculty, major, year;
	
	public Data(String n, String i, String f, String m, String y)
	{
		name = n;
		id = i;
		faculty = f;
		major = m;
		year = y;
	}
	
	public String toString()
	{
		return ("name : " + name +"\t\tid : " + id + "\tfaculty: " + faculty + "\tmajor: " + major + 
						"\tyear: " + year);
	}
	
	
}
