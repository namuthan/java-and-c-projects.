import java.awt.Container;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;


public class InsertNodeFrame extends JFrame {
	private static BinSearchTree bst;
	private JPanel buttonsPanel;
	private JButton insertButton;
	private JButton returnButton;
	private JPanel inputPanel;
	private JTextField inName;
	private JTextField inId;
	private JTextField inFac;
	private JTextField inMaj;
	private JTextField inYear;
	private JPanel labelPanel;
	private JLabel labelName;
	private JLabel labelId;
	private JLabel labelFac;
	private JLabel labelMaj;
	private JLabel labelYear;
	
	
	private static String titleFrame="Insert a New Node";

	//private InsertNodeFrame ins;// YOU MUST DEFINE THIS CLASS
	
	private Container c;
	// ADD MORE COMPONENTS AS NEEDED
	
	public InsertNodeFrame (BinSearchTree bst) {
		super(titleFrame);
		this.setLocationByPlatform(true);
		this.bst=bst;
		// create and initialize the components
		
		buttonsPanel = new JPanel();
		insertButton = new JButton("Insert");
		returnButton = new JButton("Return to Main Window");
		
		
		
		inputPanel = new JPanel();
		inName = new JTextField(10);
		inName.setActionCommand("Enter the Student's Name");
		
		/*inName.addActionListener(this);
		JLabel textFieldLabel = new JLabel("Enter the Student's Name" + ": ");
        textFieldLabel.setLabelFor(inName);
		this.add(textFieldLabel);		
		*/
		inId = new JTextField(10);
		inFac = new JTextField(10);
		inMaj = new JTextField(10);
		inYear = new JTextField(10);
		labelPanel = new JPanel();
		labelName = new JLabel("Enter the Student's Name");
		labelId = new JLabel("Enter the Student's ID");
		labelFac = new JLabel("Enter Faculty");
		labelMaj = new JLabel("Enter the Student's Major");
		labelYear = new JLabel("Enter year");
		
		
		// get a reference to the frame's container and add
		// a panel to the container
		c = getContentPane();
		
		
		// ADD MORE COMPONENTS TO THE PANELS AS NEEDED
		initComponents();
		
		// ADD MORE PANELS TO THE CONTAINER AS NEEDED
		initButtonHandlers();
		
		//create a window listener to close the program
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				dispose();
			}
			});

		
		pack();
		
		/*
		frameDimension = new Dimension(200,200);
		this.setSize(frameDimension);*/
		
	} // END OF CONSTRUCTOR
	// ADD MORE ActionPerformed METHODS AS NEEDED

	public void initComponents(){
		//initialize button panel
		buttonsPanel.add(insertButton);
		buttonsPanel.add(returnButton);
		c.add(buttonsPanel, "South");
		

		
		
		inputPanel.setLayout(new GridLayout(5,5));
		inputPanel.add(labelName);
		inputPanel.add(inName);
		
		inputPanel.add(labelId);
		inputPanel.add(inId);
		
		inputPanel.add(labelFac);
		inputPanel.add(inFac);
				
		inputPanel.add(labelMaj);
		inputPanel.add(inMaj);
		
		inputPanel.add(labelYear);
		inputPanel.add(inYear);
		
		c.add(inputPanel,"East");
		
	}
	
	public void initButtonHandlers(){
		// create an anonymous ActionListener object to
				// listen to insert-button event	
				insertButton.addActionListener(new ActionListener()
				{
					public void actionPerformed (ActionEvent evt){
						if(bst.empty()){
							JOptionPane.showMessageDialog(null, "There is no Binary Tree created. Create from file first.", "Warning", JOptionPane.ERROR_MESSAGE);
						}
						else if((inName.getText().equals(""))|| (inId.getText().equals(""))|| (inFac.getText().equals("")) || (inMaj.getText().equals("")) || (inYear.getText().equals("")))
							JOptionPane.showMessageDialog(null, "Please fill in all data fields.", "Warning", JOptionPane.ERROR_MESSAGE);
						else{
							InsertNodeFrame.bst.insert(inName.getText(), inId.getText(), inFac.getText(), inMaj.getText(), inYear.getText());
							dispose();
						}
					}
				});
				
				
				// create an anonymous ActionListener object to
				// listen to return-button event	
				returnButton.addActionListener(new ActionListener()
				{
					public void actionPerformed (ActionEvent evt){
						dispose();
					}
				});
				
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		InsertNodeFrame n = new InsertNodeFrame(new BinSearchTree());
		n.setVisible(true);
	}

}
