/*
 * Node.java
 *
 * Created on March 16, 2002, 9:59 AM
 */

// package binSearchApp;

/**
 *
 * @author  M. Moussavi
 * @version 
 */
class Node {
	
	Data data;
	Node left, right;
	
	public Node(String name, String id, String faculty, String major, String year)
	{
		data = new Data(name, id, faculty, major, year);
		left = null;
		left = null;
	}
	
	public String toString()
	{
		return data.toString();
	}
	
}

