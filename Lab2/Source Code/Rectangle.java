
public class Rectangle extends Shape{
	protected double width;
	protected double length;
	private String shapeId;
	private static int counter = 0;
	public Rectangle(String rectName, double x, double y, double width, double length) {
		
		super(rectName, x, y);
		counter++;
		shapeId="R"+ counter;
		this.width=width;
		this.length=length;
	}
	public double area(){
		/*
		 * REQUIRES: width and length are already intialized as doubles
		 * PROMISES: returns the area as a double
		 */
		return (width*length);
	}
	public double perimeter(){
		/*
		 * REQUIRES: width and length are intialized and doubles
		 * PROMISES: returns the perimeter as a double
		 */
		return (2*width + 2*length);
	}
	public double volume(){return 0;}
	
	//setters and getters specific to Rectangle
	public static int count(){return counter;}
	public int getCounter(){
		return counter;
	}
	public String getShapeId(){
		return shapeId;
	}
	public double getWidth(){
		return width;
	}
	public double getLength(){
		return length;
	}
	public void setShapeId(String str){
		shapeId=str;
	}
	public void setCounter(int count){
		counter=count;
	}
	public void setWidth(double width){
		this.width=width;
	}
	public void setLength(double length){
		this.length=length;
	}
	public String toString(){
		return ("Shape name: "+shapeName+"\nShape Id: "+ shapeId+"\nOrigin: "+origin+
				"\nWidth: "+width+"\nLength: "+length);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shape s1;
		Rectangle r1 = new Rectangle("MY Rectangle", 1.0, 2.0,2,2);
		System.out.println(r1.perimeter()+" "+r1.area());

	}

}
