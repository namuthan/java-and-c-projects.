
public class GraphicsWorld  {
	private static Shape [] shape_array;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("\nThis program has been written by: Shadda Mohammed and Braden Mund" );
		System.out.println("\nSubmitted at: 10:00 pm, January 31 , 2013\n");
		System.out.println("\nTesting methods in class Point:" );
		Point p1 = new Point(55, 66);
		Point p2 = new Point (22, 33);
		System.out.println("The distance between p1 and p2 is: " + p1.distanceBetweenPoints(p2));
		System.out.println("\nTesting methods in class Rectangle:");
		Rectangle a = new Rectangle ("RECTANGLE A",5, 7, 12, 15);
		System.out.println(a);
		System.out.println("Area: " + a.area() + " Perimeter: " + 
		a.perimeter());
		Rectangle b = new Rectangle ( "RECTANGLE B",16 , 7, 8, 9);
		System.out.println(b);
		System.out.println("Area: " + b.area() + " Perimeter: " + 
		b.perimeter());
		double d = a.distanceBetweenShapes(b);
		System.out.println("\nThe distance between two rectangles is: " +d);
		System.out.println("\nTesting methods in class Circle:");
		Circle c = new Circle ( "CIRCLE C",3, 5, 9);
		System.out.println(c);
		System.out.println("Area: " + c.area() + " Perimeter: " + 
		c.perimeter());
		d = a.distanceBetweenShapes(c);
		System.out.println("\nThe distance between rectangle a and circle c is:"  +d);
		System.out.println("\nTesting methods in class Prism:");
		Prism p = new Prism ( "PRISM P",3, 5, 9, 10, 11);
		System.out.println(c);
		System.out.println("Surface Area: " + p.area() + "\nBase Perimeter : " + p.perimeter() + "\nVolume: " + p.volume());
		d = a.distanceBetweenShapes(p);
		System.out.println("\nThe distance between rectangle a and prism p is: " +d);
		// Using array of Shape pointers:
		System.out.println("\nTesting The Concept of Polymorphism:");
		Shape shape_array[] = new Shape[4];
		shape_array[0] = a;
		shape_array[1] = b;
		shape_array [2] = c;
		shape_array [3] = p;
		System.out.println(shape_array[0]);
		System.out.println("Area: " + shape_array[0].area() + "Perimeter: " + shape_array[0].perimeter());
		System.out.println(shape_array[1]);
		System.out.println("Area: " + shape_array[1].area() + "Perimeter: " + shape_array[1].perimeter());
		System.out.println(shape_array[2]);
		System.out.println("Area: " + shape_array[2].area() + "Perimeter: " + shape_array[2].perimeter());
		System.out.println(shape_array[3]);
		System.out.println("Surface Area:" +shape_array[3].area()+"\nBase Perimeter:" + shape_array[3].perimeter() + "\nVolume: " + shape_array[3].volume());
		System.out.println("Counting the number of shapes and points:");
		System.out.println("Total number of shapes: " + Shape.count());
		System.out.println("Total number of points is: " + Point.count());
	}

}
