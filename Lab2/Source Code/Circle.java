
public class Circle extends Shape{
	protected double radius;
	private String shapeId;
	private static int counter = 0;
	public Circle(String circName, double x, double y, double radius) {
		
		super(circName, x, y);
		counter++;
		shapeId="C"+ counter;
		this.radius=radius;
	}
	public double area(){
		/*
		 * REQUIRES: radius is already intialized as double
		 * PROMISES: returns the area as a double
		 */
		return ((Math.PI)*(Math.pow(radius,2)) );
	}
	public double perimeter(){
		/*
		 * REQUIRES: radius is intialized and double
		 * PROMISES: returns the perimeter as a double
		 */
		return ((2)*(Math.PI)*(radius));
	}
	public double volume(){return 0;}
	//setters and getters specific to circle
	public int getCounter(){
		return counter;
	}
	public String getShapeId(){
		return shapeId;
	}
	public double getRadius(){
		return radius;
	}
	public void setShapeId(String str){
		shapeId=str;
	}
	public void setCounter(int count){
		counter=count;
	}
	public void setRadius(double radius){
		this.radius=radius;
	}
	public String toString(){
		return ("Shape name: "+shapeName+"\nShape Id: "+ shapeId+"\nOrigin: "+origin+
				"\nradius: "+radius);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Circle r1 = new Circle ("MY Circle", 1.0, 2.0,2.2);
		System.out.println(r1.perimeter()+" "+r1.area());
		System.out.println(r1);

	}

}
