// mystring.h
#include <iostream>
using namespace std;
#ifndef MYSTRING_H
#define MYSTRING_H

class Mystring {


 public:
	friend ostream& operator << (ostream& os, Mystring& S);
  Mystring();
  // PROMISES: Empty string object is created.

  Mystring(int n);

  Mystring(const char *s);


  ~Mystring(); // destructor

  Mystring(const Mystring&); // copy constructor

  Mystring& operator =(const Mystring& rhs); // assignment operator

  Mystring& operator +=(const Mystring& rhs); // += operator
  operator char*(); //char casting

  char operator *();
  char* operator ++(int); // ++ operator postfix
  char* operator ++(); // ++ operator prefix
  char & operator[](int index);

  char* getCursor()const;

private:
  int lengthM; // the string length - number of characters excluding \0
  char* charsM; // a pointer to the beginning of an array of characters (the storage), allocated dynamically.
  char* cursorM; // a pointer that can be moved back and forth on the array of characters (the storage)
  void memory_check(char* s);
};
#endif




