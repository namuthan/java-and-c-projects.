#include <iostream>
using namespace std;
#include "Mystring.h"

int main(void)
{
  Mystring s1 = "Maple";
  Mystring s2 = " Syrup";

  // displays Maple
  cout << "Your string s1 is: " << s1 << endl ;
    
  s1 += s2;

  // displays "Maple Syrup"
  cout << "Now after operation += your string s1 is: " << s1 << endl;

  // displays 'r'
  cout << "The ninth character of s1 is: "  << s1[8] << endl;


  // The following statement is expected to move the cursorM to the next character in cahrsM
  s1++;

  // The following statement is expected to  display the character where cursorM points to, which is 'a'.
  cout << "CursorM is now pointing at the second character, which is: "  << *s1 << endl;

  s1++;

  // The following statement is expected to display the character where cursorM points to, which is 'p'.
   cout << "CursorM is now pointing at the third character in s1, which is: "  << *s1 << endl;

  // The following statement is expected to  display 'ple Syrup', as cursorM has moved twice.
  cout << s1.getCursor() << endl;

  // Compares s1 with 'Maple Syrup', should return true, as s1 contains 'Maple Syrup'
  if(strcmp((char*)s1 , "Maple Syrup") ==0 )
	  cout << " Yes! they are the same" << endl;
  else
	  cout << "No! they are not the same." <

  cout << "\nProgram terminated successfully." <<endl;

  return 0;
}

