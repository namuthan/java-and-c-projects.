#include "Mystring.h"
#include <string.h>
#include <iostream>
using namespace std;

Mystring::Mystring()
{
  charsM = new char[1];
  memory_check(charsM);
  charsM[0] = '\0';
  cursorM = charsM;
  lengthM = 0;
}

Mystring::Mystring(const char *s)
  : lengthM(strlen(s))
{
  charsM = new char[lengthM + 1];

  memory_check(charsM);
  cursorM = charsM;
  strcpy(charsM, s);
}

Mystring::Mystring(int n)
  : lengthM(0), charsM(new char[n])
{
  memory_check(charsM);
  charsM[0] = '\0';
  cursorM = charsM;
}


Mystring::Mystring(const Mystring& source):
  lengthM(source.lengthM), charsM(new char[source.lengthM+1]), cursorM(charsM)
{
  memory_check(charsM);
  strcpy (charsM, source.charsM);
}

Mystring::~Mystring()
{
  delete [] charsM;
}

char* Mystring::getCursor() const{
	return cursorM;
}

Mystring& Mystring::operator =(const Mystring& S)
{
  if(this == &S)
    return *this;
  delete [] charsM;
  lengthM = strlen(S.charsM);
  charsM = new char [lengthM+1];
  cursorM = charsM;
  memory_check(charsM);
  strcpy(charsM,S.charsM);
  return *this;
}

//will behave as a concat operator
Mystring& Mystring::operator +=(const Mystring& S)
{
	Mystring temp(charsM);
	delete [] charsM;
	lengthM = strlen(S.charsM) + strlen(temp.charsM);
	charsM = new char [lengthM+1];
	cursorM = charsM;
	memory_check(charsM);
	strcpy(charsM,temp.charsM);
	strcat(charsM,S.charsM);
	return *this;
}

//comparison returns 1 if the two are equal
 Mystring::operator char*(){
	return charsM;
}

char Mystring::operator *(){
	return *cursorM;
}

// ++ operator postfix
char* Mystring::operator ++(int){
	cursorM++;
	return cursorM;
}
// ++ operator prefix
char* Mystring::operator ++(){
	cursorM++;
	return cursorM;
}

//the << operator
ostream& operator << (ostream& os, Mystring& S)
{
	os<< S.charsM;
	return os;
}

char & Mystring::operator[](int index){
     return charsM[index];
}

void Mystring::memory_check(char* s)
{
  if(s == 0)
    {
      cerr <<"Memory not available.";
      exit(1);
    }
}

