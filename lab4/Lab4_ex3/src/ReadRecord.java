import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
// ReadRecord.java
 
public class ReadRecord
	{

		/* This method deserializes records (objects) in the given file, 'name':
		 * 1. It opens a file for deserialization. and catches the possible 
		 *    exceptions refer to the Java API.
		 * 2. uses a while loop to read the music records. The funciton to read
		 *    objects is: readObject()
		 * 3. as it reads the objects it uses the printf funtions to display them 
		 *    on the screen.
		 * 4. It catches all possible exceptions: 
		 *       - EOFException
		 *       - ClassNotFoundException, if it cannot create a record object
		 *       - IOException
		 * 5. etc...
		 */
	private ObjectInputStream input;
		private void getRecords(String name)
		{
			/* 1. It opens a file for deserialization. and catches the possible 
		    exceptions refer to the Java API.*/
			try
			{
			input = new ObjectInputStream( new FileInputStream(name ) );
			}
			catch ( IOException ioException )
			{
			System.err.println( "Error opening file." );
			}
			
			/*2. uses a while loop to read the music records. The function to read
			 *    objects is: readObject()
			 */
			try {
				
				// object to be written to file	
				while (true)
				{
			MusicRecord record = (MusicRecord)input.readObject();
			System.out.printf( "%-10d%-28s%-16s%10.2f\n", record.getYear(),
					record.getSongName(), record.getSingerName(), record.getPurchasePrice() );
				}
			} 
			catch ( EOFException e)
				{
				System.err.println("");
				}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}		

			// YOUR CODE GOES HERE
			
		} // end method readRecords
		
		
		public static void main(String [] args)
		{
			ReadRecord d = new ReadRecord();
			d.getRecords("allSongs.ser");
		}
		
	}





