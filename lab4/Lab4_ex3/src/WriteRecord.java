import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;        
import java.io.IOException;             
import java.io.ObjectOutputStream;      
import java.util.NoSuchElementException;
import java.util.Scanner; 

public class WriteRecord {
	
	ObjectOutputStream output;
	

	/* Method openFile opens a file with the given name for object serialization.
     * Means writing objects into the file.
	 * From Java API find out about the possible exceptions and catch them, with
	 * an appropriate message.  
	 */
	public void openFile(String name)
	{
    // YOUR CODE SHOULD GO HERE
		try
		{
			output = new ObjectOutputStream(new FileOutputStream(name));
		}
		catch(IOException ioException)
		{
			System.err.println("Error opening the file");
		}

	} 
	
	
	/* This method prompts the user for the an existing text file that contains   
	 * songs information. Then reads the information from file, creates 
     * a MusicRecod object, and finally serializes the created object into a file 
     * called mySongs.ser which is already open by the method openFile. This process 
     * is continued until reaches the end of the file. 
	 */
	 
	public void addRecords()
	{
	
        // YOUR CODE GOES HERE 	
		
		/* prompt the user for a file */
		
		System.out.println("Enter a file name :");
		Scanner scanner = new Scanner (System.in);
		String filename = scanner.nextLine();
	//	System.out.println(filename);		
		File file_name = new File(filename);
		
		/* checking the file existence */
		
		if (file_name.exists())
			System.out.println("File exists");
		else{
			System.out.println("File does not exist");
			System.exit(1);
		}
		
		
		try {
			/* reading information from the file */
			
			Scanner ReadFromFile = new Scanner(file_name);
			while (ReadFromFile.hasNextLine()) {
		    //  System.out.println(ReadFromFile.nextLine());	
				String year_recorded = ReadFromFile.nextLine();
				int year = Integer.parseInt(year_recorded);
				String song = ReadFromFile.nextLine();
				String singer = ReadFromFile.nextLine();
				String purchase_price = ReadFromFile.nextLine();
				double price = Double.parseDouble(purchase_price);
				String line = ReadFromFile.nextLine();			
				// System.out.println(year+" "+song +" "+ singer+" "+price+"\n");
				
				/*create a MusicRecord object and serializes the created object into a file 
				 * called mySongs.ser
				 * */
				try
				{
				MusicRecord music_recond = new MusicRecord(year,song,singer,price);		
				output.writeObject(music_recond);
		         }
				catch ( IOException ioException )
				{System.err.println( "Error writing to file." );
				} 
				catch ( NoSuchElementException elementException )
				{
				System.err.println( "Invalid input. Please try again." );
				} 
				
		}
			} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
	} 
	
	/* This method is supposed to close the input stream that was opened in the 
     * method openFile.
     */
	public void closeFile()
	{
	
	  // YOUR CODE SHOULD GO HERE
			try
			{
				if(output != null)
					output.close();
			}
			catch(IOException ioException)
			{
				System.err.println("Error closing the file");
				System.exit(1);
			}
			
	} // end method closeFile
	
	
	public static void main(String [] args)
	{
	  WriteRecord d = new WriteRecord();
	  String filename = "mySongs.ser";
	  d.openFile(filename);
	  d.addRecords();
	  d.closeFile();
	}
}





