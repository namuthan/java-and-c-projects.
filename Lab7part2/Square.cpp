#include "Square.h"
#include <iostream>
using namespace std;
Square::Square(double x, double y, double sideL,char* sName):Shape(x,y,sName){
    side_a = sideL;
    
}

//Copy constructor
Square::Square(const Square& orig) : Shape(orig.getOrigin().getX(), orig.getOrigin().getY(), orig.getName()){
    side_a = orig.side_a;
}
//Assignment constructor
Shape& Square::operator =(Square& rhs){
    delete[] shapeName;
    origin.setX(rhs.origin.getX());
    origin.setY(rhs.origin.getY());
    shapeName = new char[strlen(rhs.shapeName)+1];
    assert(shapeName!=NULL);
    strcpy(shapeName,rhs.shapeName);
    side_a=rhs.side_a;
}

Square::~Square() {
}

double Square::area(){
    return pow(side_a,2);
}
double Square::perimeter(){
    return 4*side_a;
}

void Square::display(){
    cout<<"Square Name: "<< getName()<<endl
            <<"X-coordinate: "<<getOrigin().getX()<<endl
            <<"Y-coordinate: "<<getOrigin().getY()<<endl;
}

double Square::getSideA(){
    return side_a;
}
