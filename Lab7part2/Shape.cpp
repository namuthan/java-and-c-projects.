# include <iostream>
# include <cmath>
# include <Point.cpp>
using namespace std;
/* Shape class*/

class Shape
{
public:
	Shape (double x,double y, char* name);
	/* destructor */
	~Shape();
	Point& getOrigin();
	 char* getName();
	 void display();
	 static double distance (Shape& shape1, Shape& shape2);
	 double distance (Shape& shape1);
	 void move (double dx, double dy);
	 void setOrigin(Point origin);
 	 void setShapeNmae(char* name);
 	 char* getShapeName();

protected:
	 Point origin;
	 char* ShapeName;
};


/*	 constructor*/
	 Shape::Shape (double x,double y, char* name)
	 {
		 origin.setXY(x,y);
		 ShapeName = new char(name);		// dynamically allocate memory for ShapeName
	 }

	/*this function should return a reference to the origin Point*/
	 Point& Shape::getOrigin()
	 {
		 return this->origin;
	 }

/*	 this function returns ShapeName*/
	 char* Shape::getName()
	 {
		 return this->ShapeName;
	 }


	 void Shape::display()
	 {
		 cout<<"Shape Name : "<< this->ShapeName<<endl;
		 cout<<"X-coordinate : "<< this->getOrigin().getX()<<endl;
		 cout<<"Y-coordinate : "<< this->getOrigin().getY()<<endl;
	 }


	static double Shape::distance (Shape& shape1, Shape& shape2)
 	 {
		 return origin.DistanceBetweenTwoPoints(shape1.origin,shape2.origin);
	 }

	/* this function returns the distance between two shapes. the shape argument passed to the function is compared
	 with the shape object that called this function*/

	 double Shape::distance (Shape& shape1)
	 {
		 return this->origin.DistanceBetweenTwoPoints(shape1.origin);
	 }

	 // move function that moves the origin by dx and dy
	 void Shape::move (double dx, double dy)
		 {
			 origin.setX(origin.getX() + dx);
			 origin.setY(origin.getY() + dy);
		 }
	 // setter and getter functions
	 void Shape::setOrigin(Point origin)
	 {
		 this->origin = origin;
	 }

	 void Shape::setShapeNmae(char* name)
	 {
		 this->ShapeName = name;
	 }

	 char* Shape::getShapeName()
	 {
		 return this->ShapeName;
	 }




