#ifndef SQUARE_H
#define	SQUARE_H

#include "Shape.h"
class Square : public Shape{
public:
    Square(double,double,double,char*);
    Square(const Square& orig);
    Square& operator = (Square&);
    virtual ~Square();
    
    virtual double area();
    virtual double perimeter();
    virtual void display();
    //getters
    double getSideA();
protected:
    double side_a;
};

#endif	/* SQUARE_H */

