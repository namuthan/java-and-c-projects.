/* Point class*/

# include <iostream>
using namespace std;

class Point
{
public:
	Point (double x, double y );
	void display();
	static int Counter();
	static double  DistanceBetweenTwoPoints(Point start, Point end);
	double DistanceBetweenTwoPoints(Point end);
	double getX();
	double getY();
	void setX(double x);
	void setY(double y);
	static int getCounter();
	void setXY(double x, double y);
	int getID();
	void setID(int id);
	private:

		double x,y;
		int id = 1000;
		int static counter =0;


};



/*	 Constructor*/
	Point::Point (double x, double y )
	{
		counter++;
		this->x =x;
		this->y = y;
		id++;
	}

/*	 display function*/

	void Point::display()
	{
		cout <<"X-coordinate : "<< this->x<<endl;
		cout << "Y-coordinate :" << this->y << endl;
	}

/*	 Counter function to return the number of objects of Point*/

	static int Point::Counter()
	{
		return counter;
	}

/*	 this function that returns the distance between two functions when two points are passed as arguments*/

	static double  Point::DistanceBetweenTwoPoints(Point start, Point end)
		{
		double distance;
		//distance = sqrt(pow((start.getX()-end.getX()), 2)+pow((start.getY()-end.getY()), 2));
		distance = sqrt(pow((start.x-end.x), 2)+pow((start.y-end.y), 2));

		return distance;
		}

	 /*this function pass one point as an argument then find the distance between the point that called
	 this function and the passed point*/
	double Point::DistanceBetweenTwoPoints(Point end)
	{
		double distance;
		//distance = distance = sqrt(pow((this->x-end.getX()), 2)+pow((this->y-end.getY()), 2));
		distance = distance = sqrt(pow((this->x-end.x), 2)+pow((this->y-end.y), 2));
		return distance;

	}
	// getter and setter functions

	double Point::getX()
	{
		return x;
	}
	double Point::getY()
	{
		return y;
	}
	void Point::setX(double x)
	{
		this->x = x;
	}
	void Point::setY(double y)
	{
		this->y = y;
	}

	static int Point::getCounter()
	{
		return counter;
	}
	void Point::setXY(double x, double y)
	{
		this->x = x;
		this->y =y;
	}

	int Point::getID()
	{
		return id;
	}

	void Point::setID(int id)
	{
		this->id = id;
	}



