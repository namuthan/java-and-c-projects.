# include <iostream>
# include <Shape.cpp>
using namespace std;

class Rectangle : public Shape
{
public:
	Rectangle(double x, double y, double w,double l, char* ShapeName);
	double area();
	double perimeter();
	void display();
	double getWidth();
	double getLength();
	void setWidth(double w);
	void setLength(double l);
	char* getName();
	void setName(char* name);
	Shape getShape();
	void setShape(Shape shape);

protected:
	Shape side_b;

private:
	double width;
	double length;

};

	Rectangle::Rectangle(double x, double y, double w,double l, char* ShapeName)
	{
		this->origin.setXY(x,y);
		this->width = w;
		this->length = l;
		this->ShapeName = ShapeName;
	}

	double Rectangle::area()
	{
		return(width*length);
	}

	double Rectangle::perimeter()
	{
		return (2*width + 2*length);
	}

	void Rectangle::display()
	{
		cout<<"Rectangle Name : " <<this->ShapeName<<endl;
		cout<< "X-coordinate : "<< this->origin.x <<endl;
		cout<<"Y-coordinate : "<< this->origin.y << endl;
	}



		void Rectangle::setWidth(double w)
		{
			this ->width =w;
		}

		void Rectangle::setLength(double l)
		{
			this ->length = l;
		}

		void Rectangle::setShape(Shape shape)
		{
			side_b = shape;
		}

		char* Rectangle::getName()
		{
			return side_b.ShapeName;
		}

		void Rectangle::setName(char* name)
		{
			side_b.ShapeName = name;
		}

		double Rectangle::getWidth()
		{
			return this->width;
		}

		double Rectangle::getLength()
		{
			return this->length;
		}

		Shape Rectangle::getShape()
		{
			return side_b;
		}


		// still need copy constructor and assignment operator
