This repository contains of projects as follows:

* lab1 -creating a linked-list that contains nodes of product with a date of       shelving that products. it emphasizes the use of association classes    relationships in java.
  The classes goes as follow, lookupTable(linked-list) class that contains of a     multiple nodes (class), and each node(class) contains of product(class) and    pointer to the next node and a key that identifies that node. each product class   contains of the name and location of the product as well as the Date(class)-day,   month, year - of the shelving-date of the product.

* Lab2 - emphasizes the inheritance relationship between classes in java.
 -we have a Point(class) that has (x,y) coordinates and we have Shape(class) that uses point to identifies the origin of the shape. We have Circle and Rectangle (classes) that inherits the Shape class, and we have a Prism(class) that inherits the Rectangle(class) and we have the GraphicWorld class for testing the project.

The rest is to come soon. a bit busy nowadays, Thanks