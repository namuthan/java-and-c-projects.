
class LT_Node {
	int keyM;
	Product productM;
	LT_Node nextM;
    //constructor
	public LT_Node(int keyA, Product productA, LT_Node nextA){
		this.keyM = keyA;
		this.productM = productA;
		this.nextM = nextA;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Product m = new Product("Video Card", 2,2,1998,33);
		LT_Node q = new LT_Node(13243,m,null);
		System.out.println(q.productM);
	}

}
