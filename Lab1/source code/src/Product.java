class Product {

	private String name;
	private Date shelving_date;
	private int location;

    //constructor
	public Product(String a, int day, int month, int year, int number){
		name = a;
		shelving_date = new Date(day, month, year);
		location = number;
	}
	
    //toString method
	public String toString (){
		return ("Name: " + name + " || Sheving Date: " + shelving_date + " || Shelf #: " + location );
	}
	
	public static void main (String[] args){
		Product p = new Product("Video Card", 2,2,1998,33);
		System.out.println(p);
	}
	
    //getters and setters
	public String get_name (){
		return name;
	}
	
	public Date get_shelving_date (){
		return shelving_date;
	}
	
	public int get_location(){
		return location;
	}
	
	public void set_name (String i){
		name = i;
	}
	
	public void set_shelving_date (Date date){
		shelving_date = date;
	}
	
	public void set_location (int number){
		location = number;
	}
}
