
class Date{
	private int day;
	private int month;
	private int year;

	public Date (int a, int b, int c){
		day = a;
		month = b;
		year = c;
	}

	public String toString (){
		return (day +"/"+ month +"/"+ year);
	}

	public static void main (String[] args){
		Date d = new Date(25,7,1992);
		System.out.println(d);
		System.out.println("the day is:" + d.get_day());
		System.out.println("the month is:" + d.get_month());
		System.out.println("the year is:" + d.get_year());
	}
    //getters and settters.
	public int get_day (){
		return day;
	}

	public int get_month (){
		return month;
	}

	public int get_year (){
		return year;
	}

	public void set_day (int i){
		day = i;
	}

	public void set_month (int i){
		month = i;
	}

	public void set_year (int i){
		year = i;
	}

}