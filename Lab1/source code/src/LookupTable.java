
public class LookupTable {
	/*
     linklist that contains the nodes of products together.
     */
    private LT_Node cursorM, headM;
	private int sizeM;
	
	public LookupTable(){
		cursorM = null;
		headM = null;
		sizeM=0;
	}
	
	public int size(){
	 // PROMISES: Returns number of keys (nodes) in the table.
		cursorM=headM;
		sizeM=0;
		while(cursorM !=null){
			sizeM++;
			cursorM=cursorM.nextM;
		}
		cursorM=null;
		return sizeM;
	}

	public boolean cursor_ok(){
	  // PROMISES: Returns 1 if the cursor is attached to a 
	// key/product pair, and 0 if the cursor is in the off-
	  // list state (means if cursor is null). 
		//NOTE:I changed this to boolean to better represent its nature
		  if(cursorM==null)
			  return false;
		  else
			  return true;
	  }
	
	public int cursor_key(){
	// REQUIRES: cursor_ok() � means cursor in not on the 
	  // off-list state
	  // PROMISES: Returns key of key/product pair to which 
	// cursor is attached.
		  if(cursor_ok())
			  return cursorM.keyM;
		  else
			  return 0;
	  }
	
	public Product cursor_product(){
	  // REQUIRES: cursor_ok()� means cursor in not on the 
	  // off-list state
	  // PROMISES: Returns product in the node that cursor is
	  // is attached to.
		  if(cursor_ok())
			  return cursorM.productM;
		  else
			  return null;
	  }
	
	public void insert(int keyA, Product productA){
	  // PROMISES: If keyA matches a key in the table, the 
	  // product for that key is set equal to productA. If keyA 
	  // does not match an existing key, keyA and productA are
	  // used to create a new key/product pair in the table. In 
	  // either case, the cursor goes to the off-list state. The 
	  // nodes should be inserted non-decreasing order of the 
	  // keyM (serial numbers)
		  LT_Node nodeM = null;
		  if((headM==null) || (headM.keyM>keyA)){
			  nodeM = new LT_Node(keyA,productA,headM);
			  headM=nodeM;
			  cursorM=null;
			  return;
		  }
		  
		  LT_Node prevM=headM;
		  cursorM=headM;
		  
		  while(cursorM!=null){
			  if(cursorM.keyM == keyA){
				  cursorM.productM = productA;
				  cursorM=null;
				  return;
				  
			  }
			  else if(cursorM.keyM>keyA){
				  nodeM = new LT_Node(keyA,productA,cursorM); 
				  prevM.nextM=nodeM;
				  cursorM=null;
				  return;
			  }
			prevM = cursorM;
			cursorM = cursorM.nextM;
		  }
		  prevM.nextM=new LT_Node(keyA,productA,cursorM);
		  cursorM=null;
		  return;
		  
	  }
	
	public void find(int keyA){
	  // PROMISES: If keyA matches a key in the table, the 
	  // cursor is attached to the corresponding key/product 
	  // pair. If keyA does not match an existing key, the 
	  // cursor is put in the off-list state.
		  
		  cursorM=headM;
		  while(cursorM!=null){
			  if(cursorM.keyM==keyA){
				  return;
			  }
			  cursorM=cursorM.nextM;
		  }
		  cursorM=null;
		  return;
	  }
	
	public void remove(int keyA){
	  // PROMISES: If keyA matches a key in the table, the 
	  // corresponding key/product pair is removed from the 
	  // table (list). If keyA does not match an existing key, 
	  // the table is unchanged. In either case, the cursor 
	  // goes to the off-list state (null).  
		  
		  
		  if(headM.keyM==keyA){
			  headM=headM.nextM;
			  return;
		  }
		  
		  cursorM=headM;
		  
		  while(cursorM.nextM!=null){
			  if(cursorM.nextM.keyM==keyA){
				  cursorM.nextM= cursorM.nextM.nextM;
				  cursorM=null;
				  return;
			  }
			  cursorM=cursorM.nextM;
		  }
		  cursorM=null;
		  return;
	  }
	
	public void go_to_first(){
	  // PROMISES: If size() > 0, cursor is moved to the first 
	// key/product pair in the table.
		if(size()>0)
			cursorM=headM;
		else
			cursorM=null;
	}
	
	public void step_fwd(){
	  // REQUIRES: cursor_ok()
	// PROMISES: If cursor is at the last key/product pair in 
	// the list, cursor goes to the off-list state. Otherwise 
	  // the cursor moves forward from one pair to the next.
		 if (this.cursor_ok()){
			 cursorM=cursorM.nextM;
		 }
	  }
	
	public boolean is_empty(){
	  // PROMISES: returns true if the list is empty.
		 if(size()<=0)
			 return true;
		 else
			 return false;
	  }
	  
	  public String toString(){
	  // PROMISES: displays the information about the product cursorM is on.
		 // This is to match the print function implemented in Demo as well as
		  //allow for single product printouts
		  if(cursorM==null) return " ";
		  String concat = "";
			concat += ("Serial#: " + cursorM.keyM + " || " + cursorM.productM);  
		  return concat;
	  }
	  
	  public String toString(String arg){
		  /*REQUIRES: a string argument is passed in
		   * PROMISES: when the argument is "all" every
		   * element of the linked list is concatenated and returned.
		   * Otherwise a message is returned describing the improper use of this method 
		   */
		  if(arg=="all"){
			  String concat = "";
			  cursorM=headM;
			  while(cursorM!=null){
				concat += ("Serial#: " + cursorM.keyM + " || " + cursorM.productM + "\n");  
				cursorM=cursorM.nextM;
			  }
			  cursorM=null;
			  return concat;
		  }
		  else
			  return ("The provided arguement is not allowed. Either use \" \" or all");
	  }
	//(one product per line)
	  void destroy(){
	  // Makes all nodes free for garbage collection, and then 
	// sets headM to zero.
		  while(headM!=null)
			  headM=headM.nextM;
	  }
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LookupTable lt = new LookupTable();
		if (lt.size() != 0){
		System.out.println("\nError: Incorrect size \n");
		System.exit(1);
		}
		System.out.println("\nPrinting table just after its creation...\n");
		System.out.println(lt.toString("all"));
		System.out.println(lt);
		// Insert using new keys.
		Product a = new Product ("Video Card", 2, 11, 1998, 33);
		Product b = new Product ("Controller", 22, 10, 2008, 93);
		Product c = new Product ("RAM", 31, 9, 2007, 3);
		Product d = new Product ("Monitor", 2, 11, 1998, 83);
		lt.insert(12355,a);
		lt.insert(12345,b);
		lt.insert(12363,c); 
		lt.insert(22290,d);
		
		if(lt.size() != 4){
		System.out.println("\nError Incorrect size.\n");
		System.exit(1);
		}
		System.out.println("\nPrinting table after inserting 4 new keys...\n");
		lt.go_to_first();
		System.out.println(lt);
		System.out.println(lt.toString("all"));
	}

}
