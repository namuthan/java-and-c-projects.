
public class Dealer {
	private CardDeck gameDeck;
	public Dealer(CardDeck gameDeck){
		this.gameDeck = gameDeck;
	}
	
    //Deal cards in a round robin style starting with player1
	public void initiateGame(Player p1, Player p2){
		for(int i=0; i<4; i++){
			try {
				p1.setCard(i, gameDeck.draw());
				p2.setCard(i, gameDeck.draw());
			} catch (CardDeckException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

    // this method checks if someone has won the game yet
	public static void checkWinner(Player p) {
		if(p.wonGame()){
			System.out.println(p.getName()+" won the game with: "+p);
			System.exit(1);
		}
	}

}
