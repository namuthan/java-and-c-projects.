
import java.util.Scanner;


public class Demo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Initialize game and prompt user for player names
		System.out.println("Game Started....");
		Scanner scan = new Scanner(System.in);

		System.out.print("Please enter Player 1's name: ");
		String player1 = scan.next();
		
		System.out.print("Please enter Player 2's name: ");
		String player2 = scan.next();
		
        //instantiate the objects needed for the game
		CardDeck gameDeck = new CardDeck();
		Dealer dealer = new Dealer(gameDeck);
		Runnable p1 = new Player(player1,gameDeck);
		Runnable p2 = new Player(player2,gameDeck);
        //create the two threads to be used.
		Thread t1 = new Thread(p1);
		Thread t2 = new Thread(p2);
		
        //call the initiateGame method from dealer. This gives players 4 cards in
        //round roubin style
		System.out.println("Dealer deals FIRST four cards as follows:");
		dealer.initiateGame((Player)p1, (Player)p2);
		System.out.println(player1+"'s FIRST four cards are: "+p1);
		System.out.println(player2+"'s FIRST four cards are: "+p2);
		System.out.println("-------------------------------------------------------------");
		//start the threads
		t1.start();
		t2.start();
	
		
	}

}
