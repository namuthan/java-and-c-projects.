
import java.util.*;


public class CardDeck {
	private int DECKSIZE = 32;
	private ArrayList<Card> deckArray;
	
    //constructor for a CardDeck
	public CardDeck(){
		deckArray = new ArrayList<Card>();
		for(int i = 0; i<=7; i++){
			for(int j = 1;j<=4;j++)
				deckArray.add(new Card(i));
		}
		Collections.shuffle(deckArray);
	}
    
    //this method is used to draw a new card from the deck
    //the drawn card is no longer in the deck and is returned to the caller.
    //This method hasn't been used by the pther funtions as it doesn't
    // work seemlessly with the way threads were implemented
	public synchronized Card draw() throws CardDeckException{
		
		if(deckArray.isEmpty()){
			throw new CardDeckException("There are no more cards in the deck");
		}
		else{
		Card temp = deckArray.get(0);
		deckArray.remove(0);
		return temp;
		}
	}
	
    // this method discards a card back into the deck, adding it to the end.
    //This method wasn't used either as it didnt work seemlessly with the threads,
    // but was left in since it was the foundation for the method.
	public synchronized void discard(Card discardCard) throws CardDeckException{
		int j = 0;
		for(int i=0;i<deckArray.size();i++){
			if(deckArray.get(i).equals(discardCard))
				j++;
		}
		if(j>=4)
			throw new CardDeckException("This deck already contains 4 cards of "+ discardCard+".");
		else{
			deckArray.add(discardCard);
		}
	}
	
    //functions exaclty as the previous draw funciton did except that it deals
    // with the player directly, putting the card in their hands and displaying
    // the information on the screen
	public synchronized void draw(Player p) throws CardDeckException{
		
        //check if empty
		if(deckArray.isEmpty()){
			throw new CardDeckException("There are no more cards in the deck");
		}
		else{
		//draw card and put in the last slot of the hand (fifth card)
		Card temp = new Card(deckArray.get(0));
		p.getHand()[4]=deckArray.get(0);
		deckArray.remove(0);
		
		System.out.println(p.getName()+" is playing...");
		System.out.println(p.getName()+" draws a " + temp + " from the deck");
		}
	}
	
    //functions like the discard method above except for that it deals with the
    //player object directly. This function will take a card from the player (after getting the choice)
    // and remove it from the player's hand and put back into the deck.
	public synchronized void discard(Player p) throws CardDeckException{
        //sort deck,display, and get user's discard choice
		p.sortHand();
		System.out.println(p.getName()+"\'s five cards after DRAWING a card from deck are:"+p+"(sorted)");
		System.out.print(p.getName()+" Please discard one of the five cards in your hand:");
		Scanner scan = new Scanner(System.in);
		
		//check to see if there is an integer in the scanner
		if(scan.hasNextInt()){
			int choice = scan.nextInt();
			//we check for the choice in the hand
			
				for(int i=0;i<p.getHand().length;i++){
					if(p.getHand()[i].getCardValue()==choice){
                        //we found the card so we discard it to the deck and fix the array
						discard(p.getHand()[i]);
						for(int j=i;j<p.getHand().length-1;j++){
							p.getHand()[j]=p.getHand()[j+1];
						}
                        //we want to make sure the 5th card in hand is null
						p.getHand()[4]=null;
                        //check here to see if there is a winner
						Dealer.checkWinner(p);
						System.out.println(p.getName()+"'s cards after DISCARD are:"+p);
						System.out.println("-------------------------------------------------------------");
						return;
					}
				}
                //at this point the card hasn't been found and discard is called recursively
				System.out.println("Card choosen does not exist in hand.Please try again.");
				discard(p);
			}
		
	}
    
    //This is the method that directly runs the draw and discard function. Each player thred
    // Will make a call to this function to begin the process of drawing and discarding
	public synchronized void drawAndDiscard(Player player) {

		try {
			this.draw(player);
			this.discard(player);
		} catch (CardDeckException e) {
			
			e.printStackTrace();
		}
		
	}
    
    //This will print out the entire contents of the deck starting from the first card and ending at the last
	public String toString(){
		String concat = "";
		Card tempCard=null;
		try {
			for(int i=0; i<DECKSIZE;i++){
				tempCard = new Card(draw().getCardValue());
				concat += tempCard+" ";
				discard(tempCard);
			}
		} catch (CardDeckException e) {
			e.printStackTrace();
		}
		return concat;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		
	}

}
