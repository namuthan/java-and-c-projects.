
public class Player implements Runnable{
	private static int handSize=5;
	private String name;
	private Card[] hand;
	private Card tempCard;
	private CardDeck gameDeck;
	
	/* constructor , initializing the player's name and the game Deck*/
	public Player(String playerName,CardDeck gameDeck){
		this.gameDeck = gameDeck;
		setHand(new Card[handSize]);
		setName(playerName);
	}
/* this method is to sort the cards with the player; the concept is the same as a bubble sort
 * this works by starting to compare the first number [i] with the one next to it[i+1],
 * if ith element is larger than i+1 , swap them in order. this proccess is repeated until
 * all hand is sorted */
	
	public void sortHand(){
		for(int j=getHand().length-1;j>0;j--){
			for(int i=0; i<j;i++){
				if(getHand()[i].getCardValue()>getHand()[i+1].getCardValue()){
					//swap
					Card temp = getHand()[i+1];
					getHand()[i+1]=getHand()[i];
					getHand()[i]=temp;
				}
					
			}
		}
	}
 /* this method return an array or card numbers*/
	public Card[] getHand() {
		return hand;
	}
 /* this method return the player's name */
	public String getName() {
		return name;
	}
 /* this method returns a temp card class*/
	public Card getTempCard() {
		return tempCard;
	}
/* this method sets a new card number into the hand array and set the value to the
 * specific index */
	public void setCard(int index, Card value){
		getHand()[index]=value;
	}
/* this method sets hand value*/
	public void setHand(Card[] hand) {
		this.hand = hand;
	}
/* this method sets name*/
	public void setName(String name) {
		this.name = name;
	}
/* this method sets tempCrad*/
	public void setTempCard(Card tempCard) {
		this.tempCard = tempCard;
	}
/* this method returns a string of the card numbers in hand , if the fourth element in hand is null, 
return a string of the 4 cards only, otherwise, return a string of all 5 cards*/
	
public String toString(){
		if(getHand()[4]==null)
			return getHand()[0]+" "+getHand()[1]+" "+getHand()[2]+" "+getHand()[3]+" ";
		else
			return getHand()[0]+" "+getHand()[1]+" "+getHand()[2]+" "+getHand()[3]+" "+getHand()[4]+" ";
	}
/* this method to check if the player won the game or not; this works by comparing the 4 card numbers the player has,
 if the 4 cards are the same , return true , otherwise return false*/
	public boolean wonGame(){
		if((getHand()[0].getCardValue()==getHand()[1].getCardValue())&&(getHand()[1].getCardValue()==getHand()[2].getCardValue())&&(getHand()[2].getCardValue()==getHand()[3].getCardValue()))
				return true;
		else
			return false;
	}

/* this method to run game, while no player winning the game, the players keep drawing the discarding cards (by use of drawAndDiscard method), 
there is a thread sleep for 1 second .   */	
	public void run() {
		while(!wonGame()){
			try {
				gameDeck.drawAndDiscard(this);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
			
			
		}
	
		
	}

	/**
	 * @param args
	 */
// main function to test the class's functions
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CardDeck gameDeck = new CardDeck();
		Dealer dealer = new Dealer(gameDeck);
		Player p1 = new Player("Braden",gameDeck);
		Player p2 = new Player("Braden",gameDeck);
		dealer.initiateGame((Player)p1, (Player)p2);
		p1.run();
	}

	
	

}
