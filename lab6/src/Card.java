
public class Card {
	private int cardNumber;
	
/*	 constructors to set the value of the card number*/	
	public Card(int cardNum){
		cardNumber = cardNum;
	}
	public Card(Card cardNum){
		cardNumber = cardNum.getCardValue();
	}
	
/*	this method returns true if two cards are the same ,otherwise, return false */
	public boolean equals(Object o){
		if(((Card) o).getCardValue()==this.getCardValue())
			return true;
		else
			return false;
	}
/* this method to return the the card number */
	public int getCardValue(){
		return cardNumber;
	}
/* this method returns the card number as a string to it is used for print out on screen*/
	public String toString(){
		return ""+cardNumber;
	}
	/**
	 * @param args
	 */
	
	/* main function for testing */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Card c1 = new Card(2);
		Card c2 = new Card(2);
		
		System.out.println(c2.equals(c1));
	}

}
