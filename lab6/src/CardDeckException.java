
//this class handles exceptions from the card deck.
//Includes exceptions due to drawing too many cards, or
// discarding cards that don't belong
public class CardDeckException extends Exception{
	
	public CardDeckException(){
		super();
	}
	public CardDeckException(String message){
		super(message);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
